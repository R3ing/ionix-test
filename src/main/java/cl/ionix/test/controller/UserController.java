package cl.ionix.test.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.ionix.test.dto.UserDto;
import cl.ionix.test.entity.User;
import cl.ionix.test.service.IUserService;
import cl.ionix.test.util.MapperUtil;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService userService;

	private Map<String, Object> response;

	@GetMapping
	public ResponseEntity<List<UserDto>> findAll() {

		List<User> users = userService.findAll();

		return new ResponseEntity<>(MapperUtil.mapAll(users, UserDto.class), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Object> create(@Valid @RequestBody UserDto userDto, BindingResult result) {

		if (result.hasErrors()) {

			String errors = result.getFieldErrors().stream()
					.map(err -> "the " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.joining(", "));

			response = new HashMap<>();
			response.put("error", errors);
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}

		User user = userService.findByEmail(userDto.getEmail());

		if (user != null) {
			response = new HashMap<>();
			response.put("message", "user already exists");
			return new ResponseEntity<>(response, HttpStatus.CONFLICT);
		}

		user = userService.create(MapperUtil.map(userDto, User.class));
		return new ResponseEntity<>(MapperUtil.map(user, UserDto.class), HttpStatus.CREATED);

	}

	@GetMapping("/{email}")
	public ResponseEntity<Object> findByEmail(@PathVariable String email) {

		User user = userService.findByEmail(email);

		if (user == null) {
			response = new HashMap<>();
			response.put("message", "user not found");
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(MapperUtil.map(user, UserDto.class), HttpStatus.OK);
	}

}
