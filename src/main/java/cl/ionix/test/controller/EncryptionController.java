package cl.ionix.test.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.ionix.test.dto.EncryptionDto;
import cl.ionix.test.dto.SearchDto;
import cl.ionix.test.service.EncryptionService;

@RestController
@RequestMapping("/encryption")
public class EncryptionController {
	
	@Autowired
	EncryptionService encryptionService;
	
	private Map<String, Object> response;
	
	@PostMapping
	public ResponseEntity<Object> encryption(@RequestBody EncryptionDto encryption) {
		
		if (encryption.getParam() == null || encryption.getParam().isEmpty()) {

			response = new HashMap<>();
			response.put("error", "the is required");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		SearchDto search = encryptionService.search(encryption.getParam());		
		
		return new ResponseEntity<>(search, HttpStatus.OK);
		
	}

}
