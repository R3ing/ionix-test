package cl.ionix.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchDto {

	private Integer responseCode;
	private String description;
	private Long elapsedTime;
	private ResultDto result;

}
