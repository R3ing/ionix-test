package cl.ionix.test.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

	@NotEmpty(message = "is required")
	private String name;

	@NotEmpty(message = "is required")
	private String username;

	@NotEmpty(message = "is required")
	@Pattern(regexp = "^[a-zA-Z0-9][a-zA-Z0-9._]*@[a-zA-Z0-9]+([.][a-zA-Z]+)+$", message = "is not a valid address")
	private String email;

	@NotNull(message = "is required")
	private String phone;

}
