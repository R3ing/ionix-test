package cl.ionix.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EncryptionDto {

	private String param;
}
