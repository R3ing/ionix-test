package cl.ionix.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetailDto {

	private String email;
	private String phone_number;
}
