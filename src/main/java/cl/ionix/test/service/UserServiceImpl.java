package cl.ionix.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ionix.test.entity.User;
import cl.ionix.test.repository.IUserRepository;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserRepository userRepository;

	@Override
	public User create(User user) {		
		
		return userRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User findByEmail(String email) {

		Optional<User> user = userRepository.findByEmail(email);
		return user.orElse(null);
	}

}
