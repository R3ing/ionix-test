package cl.ionix.test.service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cl.ionix.test.dto.SearchDto;

@Service
public class EncryptionServiceImpl implements EncryptionService {

	@Autowired
	RestTemplate restTemplate;

	@Override
	public SearchDto search(String param) {

		String url = "https://sandbox.ionix.cl/test-tecnico/search?rut=" + encryption(param);

		long startTime = System.currentTimeMillis();
		ResponseEntity<SearchDto> response = restTemplate.exchange(url, HttpMethod.GET, null, SearchDto.class);
		long endTime = System.currentTimeMillis();
		
		if (response.getBody() != null) {
			response.getBody().setElapsedTime(endTime - startTime);
			response.getBody().getResult().setRegisterCount(response.getBody().getResult().getItems().size());			
			response.getBody().getResult().setItems(null);

			return response.getBody();
		}

		return null;
	}

	public String encryption(String plainRut) {

		try {
			DESKeySpec keySpec = new DESKeySpec("ionix123456".getBytes(StandardCharsets.UTF_8));
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			byte[] cleartext = plainRut.getBytes(StandardCharsets.UTF_8);
			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(Cipher.ENCRYPT_MODE, keyFactory.generateSecret(keySpec));
			
			return Base64.getEncoder().encodeToString(cipher.doFinal(cleartext));
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

}
