package cl.ionix.test.service;

import cl.ionix.test.dto.SearchDto;

public interface EncryptionService {
	
	SearchDto search(String param);

}
