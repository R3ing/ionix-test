package cl.ionix.test.service;

import java.util.List;

import cl.ionix.test.entity.User;

public interface IUserService {

	User create(User user);

	List<User> findAll();

	User findByEmail(String email);

}
