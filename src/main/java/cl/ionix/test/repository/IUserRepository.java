package cl.ionix.test.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cl.ionix.test.entity.User;

@Repository
public interface IUserRepository extends CrudRepository<User, Long> {

	Optional<User> findByEmail(String email);

}
