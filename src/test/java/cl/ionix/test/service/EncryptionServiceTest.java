package cl.ionix.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import cl.ionix.test.dto.DetailDto;
import cl.ionix.test.dto.ItemDto;
import cl.ionix.test.dto.ResultDto;
import cl.ionix.test.dto.SearchDto;

public class EncryptionServiceTest {

	@Mock
	RestTemplate restTemplate;

	@InjectMocks
	EncryptionServiceImpl encryptionService;

	public static final SearchDto SEARCH_DTO = new SearchDto();

	public static final String PARAM = "1-9";

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		SEARCH_DTO.setResponseCode(0);
		SEARCH_DTO.setDescription("OK");
		SEARCH_DTO.setElapsedTime(245L);

		ResultDto result = new ResultDto();
		result.setRegisterCount(3);

		ItemDto item = new ItemDto();
		item.setName("John");

		DetailDto detail = new DetailDto();
		detail.setEmail("jdoe@gmail.com");
		detail.setPhone_number("+130256897875");

		item.setDetail(detail);

		List<ItemDto> items = new ArrayList<>();
		items.add(item);

		result.setItems(items);

		SEARCH_DTO.setResult(result);
	}

	@Test
	public void search() {

		String url = "https://sandbox.ionix.cl/test-tecnico/search?rut=" + encryptionService.encryption(PARAM);

		Mockito.when(restTemplate.exchange(url, HttpMethod.GET, null, SearchDto.class))
				.thenReturn(new ResponseEntity<SearchDto>(SEARCH_DTO, HttpStatus.OK));

		SearchDto search = encryptionService.search(PARAM);

		assertEquals(SEARCH_DTO, search);
	}

	@Test
	public void searchNoBody() {

		String url = "https://sandbox.ionix.cl/test-tecnico/search?rut=" + encryptionService.encryption(PARAM);

		Mockito.when(restTemplate.exchange(url, HttpMethod.GET, null, SearchDto.class))
				.thenReturn(new ResponseEntity<SearchDto>(HttpStatus.OK));

		SearchDto search = encryptionService.search(PARAM);

		assertNull(search);
	}

	@Test
	public void encryption() {
		String expected = "FyaSTkGi8So=";
		String actual = encryptionService.encryption(PARAM);

		assertEquals(expected, actual);
	}

	@Test(expected = RuntimeException.class)
	public void Exception() throws RuntimeException {

		encryptionService.encryption(null);

		fail();
	}

}
