package cl.ionix.test.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import cl.ionix.test.entity.User;
import cl.ionix.test.repository.IUserRepository;

public class UserServiceTest {

	@Mock
	IUserRepository userRepository;

	@InjectMocks
	UserServiceImpl userService;
	
	private static final List<User> USERS = new ArrayList<>();

	private static final User USER = new User();

	private static final String EMAIL = "user@email.com";

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		
		USER.setName("user");
		USER.setUsername("username");
		USER.setEmail(EMAIL);
		USER.setPhone("+56912345678");

		USERS.add(USER);
	}
	
	@Test
	public void create() {
		
		Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(new User());
		
		userService.create(USER);
	}
	
	@Test
	public void findAll() {
		
		Mockito.when(userRepository.findAll()).thenReturn(USERS);
		
		List<User> users = userService.findAll();
		
		assertEquals(USERS, users);
	}
	
	@Test
	public void findByEmail() {
		
		Mockito.when(userRepository.findByEmail(EMAIL)).thenReturn(Optional.of(USER));
		
		userService.findByEmail(EMAIL);
	}

}
