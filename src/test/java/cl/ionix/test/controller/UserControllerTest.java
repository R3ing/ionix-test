package cl.ionix.test.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import cl.ionix.test.dto.UserDto;
import cl.ionix.test.entity.User;
import cl.ionix.test.service.IUserService;
import cl.ionix.test.util.MapperUtil;

public class UserControllerTest {

	@Mock
	IUserService userService;

	@Mock
	BindingResult bindingResult;

	@InjectMocks
	UserController userController;

	private static final List<User> USERS = new ArrayList<>();

	private static final User USER = new User();

	private static final String EMAIL = "user@email.com";

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		USER.setName("user");
		USER.setUsername("username");
		USER.setEmail(EMAIL);
		USER.setPhone("+56912345678");

		USERS.add(USER);
	}

	@Test
	public void findAll() {
		Mockito.when(userService.findAll()).thenReturn(USERS);

		ResponseEntity<List<UserDto>> response = userController.findAll();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertFalse(response.getBody().isEmpty());
	}

	@Test
	public void create() {

		Mockito.when(userService.findByEmail(EMAIL)).thenReturn(null);

		Mockito.when(userService.create(USER)).thenReturn(USER);

		ResponseEntity<Object> response = userController.create(MapperUtil.map(USER, UserDto.class), bindingResult);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}

	@Test
	public void createBadRequest() {

		Mockito.when(bindingResult.hasErrors()).thenReturn(true);

		ResponseEntity<Object> response = userController.create(MapperUtil.map(USER, UserDto.class), bindingResult);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void createUserAlreadyExists() {

		Mockito.when(userService.findByEmail(EMAIL)).thenReturn(USER);

		ResponseEntity<Object> response = userController.create(MapperUtil.map(USER, UserDto.class), bindingResult);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void findByEmail() {

		Mockito.when(userService.findByEmail(EMAIL)).thenReturn(USER);

		ResponseEntity<Object> response = userController.findByEmail(EMAIL);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(USER.getEmail(), ((UserDto) response.getBody()).getEmail());

	}

	@Test
	public void findByEmailNotFound() {

		Mockito.when(userService.findByEmail(EMAIL)).thenReturn(null);

		ResponseEntity<Object> response = userController.findByEmail(EMAIL);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

	}

}
