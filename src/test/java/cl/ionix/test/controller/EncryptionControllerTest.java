package cl.ionix.test.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import cl.ionix.test.dto.EncryptionDto;
import cl.ionix.test.dto.ResultDto;
import cl.ionix.test.dto.SearchDto;
import cl.ionix.test.service.EncryptionService;

public class EncryptionControllerTest {

	@Mock
	EncryptionService encryptionService;

	@InjectMocks
	EncryptionController encryptionController;
	
	public static final EncryptionDto ENCRYPTION_DTO = new EncryptionDto();
	
	public static final SearchDto SEARCH_DTO = new SearchDto();
	
	public static final String PARAM = "1-9";

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		
		SEARCH_DTO.setResponseCode(0);
		SEARCH_DTO.setDescription("OK");
		SEARCH_DTO.setElapsedTime(245L);
		
		ResultDto result = new ResultDto();
		result.setRegisterCount(3);
		
		SEARCH_DTO.setResult(result);
	}
	
	@Test
	public void encryption() {
		
		ENCRYPTION_DTO.setParam(PARAM);
		
		Mockito.when(encryptionService.search(PARAM)).thenReturn(SEARCH_DTO);
		
		ResponseEntity<?> response = encryptionController.encryption(ENCRYPTION_DTO);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(SEARCH_DTO.getDescription(), ((SearchDto)response.getBody()).getDescription());
	}
	
	@Test
	public void encryptionBadRequestNull() {
		
		ENCRYPTION_DTO.setParam(null);
		
		ResponseEntity<?> response = encryptionController.encryption(ENCRYPTION_DTO);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());		
	}
	
	@Test
	public void encryptionBadRequestEmpty() {
		
		ENCRYPTION_DTO.setParam("");
		
		ResponseEntity<?> response = encryptionController.encryption(ENCRYPTION_DTO);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());		
	}

}
